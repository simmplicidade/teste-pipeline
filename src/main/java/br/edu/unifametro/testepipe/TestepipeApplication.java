package br.edu.unifametro.testepipe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestepipeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestepipeApplication.class, args);
	}

}
